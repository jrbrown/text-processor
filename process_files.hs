import System.Environment (getArgs)
import Data.Maybe (fromMaybe, mapMaybe)
import Text.Read (readMaybe)
import qualified Data.Map as M
import qualified Data.List as L


--Useful type annotations
type Option = Char
type Arg = String
type Suffix = String
type OptionData = ([(Option, Arg)], [Option], [Arg])

type StrFunc = (String -> String)
type StrFilter = (String -> Bool)
type FilterInvert = Bool
type NewLineFlag = Bool
type NameOnlyFlag = Bool
type NameAtStartFlag = Bool
data OutputMode = Modify Suffix
                | Print

data Config = Config
              [FilePath] StrFunc StrFilter
              FilterInvert NewLineFlag NameOnlyFlag NameAtStartFlag
              OutputMode


noArgError :: Char -> String
noArgError x = "Option \'" ++ [x] ++ "\' required an argument but none was given"

tooManyReqArgOptionsError :: String -> String
tooManyReqArgOptionsError xs = "Options \'" ++ xs ++ "\' were all given at once but all require an argument"

-- Get all possible options with their own args from the args
-- Takes a list of all options which require an arg
-- Returns a list of any req_arg_options with their args, other options found
-- and all leftover args
getOptionData :: String -> [String] -> Either String OptionData
getOptionData req_arg_options args = getOptionData' args ([], [], [])
  where
    getOptionData' :: [String] -> OptionData -> Either String OptionData
    getOptionData' [] result = Right result

    getOptionData' [x] (aos, os, as)
      | head x == '-'  = case tail x `L.intersect` req_arg_options of
                           ""  -> Right (aos, os ++ tail x, as)
                           [y] -> Left $ noArgError y
                           ys  -> Left $ tooManyReqArgOptionsError ys
      | otherwise   = Right (aos, os, as++[x])

    getOptionData' (x1:x2:xss) (aos, os, as)
      | head x1 == '-' = case tail x1 `L.intersect` req_arg_options of
                 ""  -> getOptionData' (x2:xss) (aos, os ++ tail x1, as)
                 [y] -> case head x2 of
                          '-' -> Left $ noArgError y
                          _   -> if y `elem` map fst aos
                                    then Left $ "Option \'" ++ [y] ++ "\' was given more than once"
                                    else getOptionData' xss (aos ++ [(y, x2)], os ++ L.delete y (tail x1), as)
                 ys  -> Left $ tooManyReqArgOptionsError ys
      | otherwise      = getOptionData' (x2:xss) (aos, os, as ++ [x1])


readArgs :: [String] -> Either String Config
readArgs args = do
  (aos, os, as) <- getOptionData "mfs" args
  let aoms = M.fromList aos

  -- Check an operation is specified
  assertErr "No operation specified, use -m and/or -f" (M.member 'm' aoms || M.member 'f' aoms)

  let getFromMap x m = maybeToRight ("\'" ++ x ++ "\' not found in the map") (M.lookup x m)

  let m = M.findWithDefault "id" 'm' aoms
  func <- m `getFromMap` strFuncMap

  let f = M.findWithDefault "constTrue" 'f' aoms
  filt <- f `getFromMap` strFilterMap

  let isO x = x `elem` os

  let pcfg = Config as func filt ('i' `elem` os) ('q' `elem` os) ('l' `elem` os) (isO 't')
  case (M.lookup 's' aoms, 'p' `elem` os) of
    (Just _, True)  -> Left "Can't give -p and -s"
    (Nothing, True) -> Right $ pcfg Print
    (Just sfx, False)  -> Right $ pcfg (Modify sfx)
    (Nothing, False) -> Left "Need to give one of -p or -s"


helpString :: String
helpString = unlines [ "Haskell File Processor"
                     , ""
                     , "Note: Work in progress"
                     , ""
                     , "Options:"
                     , "  -m [arg]    Function to apply file contents"
                     , "  -f [arg]    Filter to apply to outputted files based on contents"
                     , "  -s [arg]    File write file name suffix"
                     , "  -n [arg]    File write file name function (Unimplemented)"
                     , "  -d [arg]    File write directory (Unimplemented)"
                     , "  -i          Invert the filter"
                     , "  -q          Add space between file outputs when printing"
                     , "  -l          Only print file names"
                     , "  -p          Print output"
                     , "  -t          Add file name to top of file contents before processing"
                     , ""
                     , "Usage restrictions:"
                     , "  Requires at least one of: m f "
                     , "  Requires exactly one of: p s n"
                     , "  d is optional when using s or n"
                     , "  i is optional when using f"
                     , "  q and l are optional when using p"]


splitHeadRecombine :: (Monoid a) => [a] -> (a, a)
splitHeadRecombine xs = (head xs, mconcat $ tail xs)


maybeToRight :: a -> Maybe b -> Either a b
maybeToRight x Nothing  = Left x
maybeToRight x (Just y) = Right y


assertErr :: a -> Bool -> Either a ()
assertErr x b = if not b then Left x else Right ()


mkListOpSafe :: ([a] -> a) -> [a] -> Maybe a
mkListOpSafe f [] = Nothing
mkListOpSafe f xs = Just $ f xs

safeLast = mkListOpSafe last
safeHead = mkListOpSafe head
safeMaximum = mkListOpSafe maximum


funcOp :: (Functor f) => f (a -> b) -> (f b -> c) -> a -> c
funcOp ops comb arg = comb $ fmap ($ arg) ops


noInfoStr :: String
noInfoStr = "~~~ No information found ~~~"


linesByInfxAll :: [String] -> String -> Bool
linesByInfxAll infxs = funcOp (map L.isInfixOf infxs) and


linesByInfxAny :: [String] -> String -> Bool
linesByInfxAny infxs = funcOp (map L.isInfixOf infxs) or


getItem :: (Read a) => String -> String -> Maybe a
getItem infx = readMaybe . filterComma . (!! 1) . dropWhile (not . L.isInfixOf infx) . words
  where filterComma txt = if last txt == ',' then init txt else txt


getWithWithout :: (Read a) => String -> [String] -> [String] -> String -> [a]
getWithWithout tgt reqs i_reqs = mapMaybe (getItem tgt) . f2 . f1 . lines
    where
      f1 = filter (linesByInfxAll (tgt : reqs))
      f2 = filter (not . linesByInfxAny i_reqs)

getEvalAccs :: String -> [Double]
getEvalAccs = getWithWithout "acc" ["eval"] []

getTrainAccs :: String -> [Double]
getTrainAccs = getWithWithout "acc" ["train"] ["eval"]

getEvalLoss :: String -> [Double]
getEvalLoss = getWithWithout "loss" ["eval"] []

getTrainLoss :: String -> [Double]
getTrainLoss = getWithWithout "loss" ["train"] ["eval"]


filterErrorLines :: String -> String
filterErrorLines = unlines . filter (linesByInfxAny ["error", "Error", "ERROR"]) . lines


createDBEntryFromLog :: String -> String
createDBEntryFromLog txt = init $ unlines output_lines  -- init to remove trailing \n
  where
    f_name = (takeWhile (/='.') . head . lines) txt
    f_body = (unlines . tail . lines) txt
    eval_acc_vals = getEvalAccs f_body
    formatListVal val = "                " ++ show val ++ ","
    formatListVals = init . init . unlines . map formatListVal  -- inits to remove trailing ,\n
    output_lines =
      [ "\"" ++ f_name ++ "\": {"
      , "    \"model_dir\": null,"
      , "    \"config\": null,"
      , "    \"history\": {"
      , "        \"train\": {"
      , "            \"loss\": ["
      , formatListVals (getTrainLoss f_body) 
      , "            ],"
      , "            \"accuracy\": ["
      , formatListVals (getTrainAccs f_body)
      , "            ]"
      , "        },"
      , "        \"validation\": {"
      , "            \"loss\": ["
      , formatListVals (getEvalLoss f_body) 
      , "            ],"
      , "            \"accuracy\": ["
      , formatListVals (getEvalAccs f_body)
      , "            ]"
      , "        }"
      , "    }"
      , "},"]


strFuncMap :: M.Map String (String -> String)
strFuncMap = M.fromList [ ("id", id)
                        , ("valAccs", unlines . map show . getEvalAccs)
                        , ("finalValAcc", maybe noInfoStr show . safeLast . getEvalAccs) 
                        , ("bestValAcc", maybe noInfoStr show . safeMaximum . getEvalAccs)
                        , ("lastLine", fromMaybe noInfoStr . safeLast . lines)
                        , ("errors", filterErrorLines)
                        , ("createDBEntry", createDBEntryFromLog) ]


strFilterMap :: M.Map String (String -> Bool)
strFilterMap = M.fromList [ ("constTrue", const True) 
                          , ("infoOnly", (/=noInfoStr) ) ]


addSfxToFName :: Suffix -> FilePath -> FilePath
addSfxToFName sfx fname = file_root ++ sfx ++ file_ext
  where (file_root, file_ext) = break (=='.') fname


processConfig :: Config -> IO ()
processConfig (Config fpaths func filt filt_inv nl name_only name_at_start out_mode) = do

  let func' n = if name_at_start
                 then func . (++) (n ++ ['\n'])
                 else func

  file_data <- mapM (\name -> sequence (name, func' name <$> readFile name)) fpaths
  let filt_file_data = filter (\(_, dat) -> filt dat /= filt_inv) file_data

  case out_mode of
    Modify sfx -> mapM_ (\(name, dat) -> writeFile (addSfxToFName sfx name) dat) filt_file_data
    Print -> mconcat $ map (putStrLn . process_file) filt_file_data
      where
        nl_txt = if nl then "\n" else ""
        process_file (fname, dat) =
          if name_only
             then fname
             else dat ++ nl_txt


runWithArgs :: [String] -> IO ()
runWithArgs args = case readArgs args of
                     Left err  -> putStrLn $ "Error: " ++ err
                     Right cfg -> processConfig cfg


main :: IO ()
main = do
  args <- getArgs
  case args of
    [] -> putStrLn "No args were given, try -h or help"
    a@[x] -> if x == "-h" || x == "help"
                then putStrLn helpString
                else runWithArgs a
    a -> runWithArgs a

