module Main (main) where


import ArgParsing (execArgParser)
import TextProcessing (runConfig)


main :: IO ()
main = runConfig =<< execArgParser

