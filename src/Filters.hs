module Filters
  ( fdatFilterMap
  , fdatArgFilterMap ) where


import Common (noInfoStr, FileData)

import Data.Map as M
import Data.Bifunctor (second)
import Text.Regex.PCRE ((=~))


fdatFilterMap :: M.Map String (FileData -> Bool)
fdatFilterMap = fmap snd . second <$> strFilterMap


strFilterMap :: M.Map String (String -> Bool)
strFilterMap = M.fromList
  [ ("constTrue", const True) 
  , ("constFalse", const False)
  , ("infoOnly", (/= noInfoStr) ) ]


fdatArgFilterMap :: M.Map String (String -> FileData -> Bool)
fdatArgFilterMap = fmap (snd <$>) . (second .) <$> strArgFilterMap


strArgFilterMap :: M.Map String (String -> String -> Bool)
strArgFilterMap = M.fromList
  [ ("grep", flip (=~)) ]

