module LogParse
    ( Block (..)
    , NASLog (..)
    , nasLogParse
    , nasLogP
    , runNameP
    , blockLayersP
    ) where


import Common (sglListToEither)
import RunInfo (Attn (..), attnP)
import Text.ParserCombinators.ReadP
import Text.ParserCombinators.SimpleJSON


data Block = Block Attn Int deriving (Eq, Ord, Show)
data NASLog = NASLog String [[Block]] deriving (Eq, Ord, Show)


nasLogParse :: String -> Either String NASLog
nasLogParse = sglListToEither . containsParse nasLogP


blockLayersP :: ReadP [[Block]]
blockLayersP = do
    string "Current blocks in each layer"
    whiteSpaces
    char '['
    whiteSpaces
    blks <- many1 blockLayerP
    whiteSpaces
    char ']'
    return blks


blockLayerP :: ReadP [Block]
blockLayerP = do
    char '['
    whiteSpaces
    vals <- sepBy blockP commaSep
    whiteSpaces
    char ']'
    return vals


blockP :: ReadP Block
blockP = do
    optional $ choice [char '\'', char '"']
    attn <- attnP
    num <- option 0 (optional (char '_') >> int)
    optional $ choice [char '\'', char '"']
    return $ Block attn num


nasLogP :: ReadP NASLog
nasLogP = do
    name <- skipTill runNameP
    blk_lyrs <- getLast blockLayersP
    return $ NASLog name blk_lyrs


runNameP :: ReadP String
runNameP = do
    string "Run name:"
    whiteSpaces
    str <- notWhiteSpaces
    choice [string ",", many1 whiteSpace]
    return str

