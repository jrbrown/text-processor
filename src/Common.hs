{-# LANGUAGE ScopedTypeVariables #-}

module  Common
  ( Suffix
  , FileName (..)
  , FileBody
  , FileData
  , (<$$>)
  , (<$$$>)
  , headTailOp
  , headTailOp2
  , numMeanStdDev
  , numMeanStdDevFromMany
  , sglListToEither
  , failEither
  , maybeFail
  , changeFileExt
  , intercalateM
  , ks2asFrom2Maps
  , funcOp
  , applyFuncToFileFameRoot
  , maybeToRight
  , assertErr
  , safeLast
  , safeHead
  , safeInit
  , safeTail
  , safeMaximum
  , safeMaximumBy
  , noInfoStr
  , roundFloatStr
  ) where


import qualified Data.Map as M
import Data.List (intersperse, maximumBy)
import Text.Printf (PrintfArg, printf)
import GHC.Utils.Misc (fstOf3)


type Suffix = String
data FileName = Str String | StdInOut deriving (Eq, Ord)
type FileBody = String
type FileData = (FileName, FileBody)


instance Show FileName where
  show (Str x)  = x
  show StdInOut = "StdInOut"

instance Semigroup FileName where
  (<>) (Str n1) (Str n2) = Str (n1 <> n2)
  (<>) (Str n1) StdInOut = Str n1
  (<>) StdInOut (Str n2) = Str n2
  (<>) StdInOut StdInOut = StdInOut

instance Monoid FileName where
  mempty = StdInOut


(<$$>) :: (Functor f, Functor g) => (a -> b) -> f (g a) -> f (g b)
(<$$>) f x = fmap f <$> x

(<$$$>) :: (Functor f, Functor g, Functor h) => (a -> b) -> f (g (h a)) -> f (g (h b))
(<$$$>) f x = fmap f <$$> x


headTailOp :: (a -> b) -> (a -> b) -> [a] -> [b]
headTailOp _ _ [] = []
headTailOp op_head _ [x] = [op_head x]
headTailOp op_head op_tail (x:xs) = op_head x : (op_tail <$> xs)


headTailOp2 :: (a -> b) -> (a -> b) -> [[a]] -> [[b]]
headTailOp2 op_head op_tail = headTailOp (headTailOp op_head op_tail) (op_tail <$>)


numMeanStdDev :: [Double] -> (Int, Double, Double)
numMeanStdDev nums = (n, mean, sqrt var)
  where
    n = length nums
    mean = sum nums / realToFrac n
    var = (sxsq / realToFrac n) - (mean ** 2)
    sxsq = sum (map (**2) nums)


numMeanStdDevFromMany :: [(Int, Double, Double)] -> (Int, Double, Double)
numMeanStdDevFromMany num_means_stdevs = (n, mean, sqrt var)
  where
    n = sum $ fstOf3 <$> num_means_stdevs
    mean = sx / realToFrac n
    var = (sxsq / realToFrac n) - (mean ** 2)
    sx = foldr (\(ni, mi, _) sxi -> (realToFrac ni * mi) + sxi) 0 num_means_stdevs
    sxsq = foldr (\(ni, mi, si) sxsqi -> (realToFrac ni * ((si ** 2) + (mi ** 2))) + sxsqi ) 0 num_means_stdevs


sglListToEither :: [a] -> Either String a
sglListToEither [] = Left "No elements"
sglListToEither [x] = Right x
sglListToEither _ = Left "Too many elements"


failEither :: Either String a -> a
failEither (Left err) = error err
failEither (Right x) = x


maybeFail :: String -> Maybe a -> a
maybeFail str Nothing = error str
maybeFail _ (Just x) = x


changeFileExt :: FileName -> String -> FileName
changeFileExt StdInOut _ = StdInOut
changeFileExt (Str name) ext = Str (takeWhile (/= '.') name ++ "." ++ ext)


intercalateM :: (Monoid a) => a -> [a] -> a
intercalateM x = mconcat . intersperse x


ks2asFrom2Maps :: forall k a. (Show k, Ord k) => String -> M.Map k a -> M.Map k (k -> a) -> [k]
               -> Either String [a]
ks2asFrom2Maps m_name map1 map2 ks = sequence $ getMult ks
  where
    getSingle key = maybeToRight (show key ++ " not a valid " ++ m_name) (M.lookup key map1)
    getMult :: [k] -> [Either String a]
    getMult [] = []
    getMult [x] = [getSingle x]
    getMult (x1:x2:xs) = case getDouble x1 x2 of
                           Just y  -> Right y : getMult xs
                           Nothing -> getSingle x1 : getMult (x2:xs)
    getDouble :: k -> k -> Maybe a
    getDouble k1 k2 = fmap ($ k2) (M.lookup k1 map2)


funcOp :: (Functor f) => f (a -> b) -> (f b -> c) -> a -> c
funcOp ops comb arg = comb $ fmap ($ arg) ops


applyFuncToFileFameRoot :: (String -> String) -> (FileName -> FileName)
applyFuncToFileFameRoot _ StdInOut = StdInOut
applyFuncToFileFameRoot func (Str name) = Str $ func file_root ++ file_ext
  where (file_root, file_ext) = break (== '.') name


maybeToRight :: a -> Maybe b -> Either a b
maybeToRight x Nothing  = Left x
maybeToRight _ (Just y) = Right y


assertErr :: a -> Bool -> Either a ()
assertErr x b = if not b then Left x else Right ()


mkListOpSafe :: ([a] -> b) -> [a] -> Maybe b
mkListOpSafe _ [] = Nothing
mkListOpSafe f xs = Just $ f xs

safeLast :: [a] -> Maybe a
safeLast = mkListOpSafe last

safeHead :: [a] -> Maybe a
safeHead = mkListOpSafe head

safeMaximum :: (Ord a) => [a] -> Maybe a
safeMaximum = mkListOpSafe maximum

safeInit :: [a] -> Maybe [a]
safeInit = mkListOpSafe init

safeTail :: [a] -> Maybe [a]
safeTail = mkListOpSafe tail

safeMaximumBy :: (a -> a -> Ordering) -> [a] -> Maybe a
safeMaximumBy f = mkListOpSafe (maximumBy f)


noInfoStr :: String
noInfoStr = "~~~ No information found ~~~"


roundFloatStr :: (PrintfArg a) => Int -> a -> String
roundFloatStr sig_fig = printf ("%0." ++ show sig_fig ++ "f" )
