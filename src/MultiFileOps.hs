module MultiFileOps
  ( MultiFileOp
  , getMOps
  ) where


import Common (FileData, ks2asFrom2Maps)
import Functions (fdatFuncMap, fdatArgFuncMap)
import Filters (fdatFilterMap, fdatArgFilterMap)

import Data.Map as M

type MultiFileOp = String


getMOps :: [String] -> Either String [[FileData] -> [FileData]]
getMOps = ks2asFrom2Maps "function or filter" fdatMap fdatArgMap
  where
    fdatMap = M.union (fmap <$> fdatFuncMap) (Prelude.filter <$> fdatFilterMap)
    fdatArgMap = M.union ((fmap .) <$> fdatArgFuncMap) ((Prelude.filter .) <$> fdatArgFilterMap)

