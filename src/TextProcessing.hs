module TextProcessing (runConfig) where


import ArgParsing
    ( Config (debugConfig, optCommand)
    , Command (One2One, Many2One, Many2Many)
    , One2OneCfg (One2OneCfg)
    , Many2OneCfg (Many2OneCfg)
    , Many2ManyCfg (Many2ManyCfg)
    , SingleFileOp )

import Common (FileData, FileName (StdInOut))
import Functions (getFuncs)
import MultiFileOps (MultiFileOp, getMOps)
import Combiners (Combiner, getCombiner)
import IO (readInput, writeOutput)


applyOperations :: a -> [a -> a] -> a
applyOperations = foldl (\v op -> op v)


runConfig :: Config -> IO ()
runConfig cfg
  | debugConfig cfg = print cfg
  | otherwise       = runCommand (optCommand cfg)


runCommand :: Command -> IO ()
runCommand (One2One cfg) = runOne2One cfg
runCommand (Many2One cfg) = runMany2One cfg
runCommand (Many2Many cfg) = runMany2Many cfg


runOne2One :: One2OneCfg -> IO ()
runOne2One (One2OneCfg inputH ops outputH) = do
  input_data <- readInput inputH :: IO FileData
  let input_name = fst input_data
  case getFuncs ops of
    Left err -> putStrLn err
    Right fs -> do
      let (output_name', output_body) = applyOperations input_data fs
      let output_name = if output_name' == input_name then StdInOut else output_name'
      writeOutput (output_name, output_body) outputH


runMany2One :: Many2OneCfg -> IO ()
runMany2One (Many2OneCfg inputH mops comb sops outputH) = do
  input_datas <- readInput inputH :: IO [FileData]
  case m2oInternal input_datas mops comb sops of
    Left err -> putStrLn err
    Right (_, output_body) -> writeOutput output_body outputH


m2oInternal :: [FileData] -> [MultiFileOp] -> Combiner -> [SingleFileOp] -> Either String FileData
m2oInternal input_datas mops comb sops = do
  ms <- getMOps mops
  let post_mops = applyOperations input_datas ms
  c <- getCombiner comb
  let post_c_data = c post_mops
  fmap (applyOperations post_c_data) (getFuncs sops)


runMany2Many :: Many2ManyCfg -> IO ()
runMany2Many (Many2ManyCfg inputH mops outputH) = do
  input_datas <- readInput inputH :: IO [FileData]
  case getMOps mops of
    Left err -> putStrLn err
    Right ms -> do
      let output_data = applyOperations input_datas ms
      writeOutput output_data outputH

