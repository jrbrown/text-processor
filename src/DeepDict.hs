{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}

module DeepDict
    ( Dict1
    , Dict2
    , Dict3
    , Dict4
    , Dict5
    , Convertable
    , convert
    , Cleanable
    , cleanMaybe
    , cleanEither
    , fetch
    ) where


import qualified Data.Map as M
import Data.Maybe (mapMaybe, catMaybes)
import Data.Bifunctor (second)
import GHC.Data.Maybe (rightToMaybe)
import Control.Monad (join)


type Dict1 a = M.Map String a
type Dict2 a = M.Map String (Dict1 a)
type Dict3 a = M.Map String (Dict2 a)
type Dict4 a = M.Map String (Dict3 a)
type Dict5 a = M.Map String (Dict4 a)

type String1 = String
type String2 = (String, String)
type String3 = (String, String, String)
type String4 = (String, String, String, String)
type String5 = (String, String, String, String, String)


class Convertable a b where
    convert :: a -> b

instance Convertable a a where
    convert = id

instance (Convertable a b) => Convertable (M.Map String (Maybe a)) (M.Map String b) where
    convert = M.fromList . fmap (second convert) . mapMaybe sequence . M.toList


class Functor f => Cleanable f where
    cleanMaybe :: f (Maybe a) -> f a
    cleanEither :: f (Either a b) -> f b
    cleanEither f = cleanMaybe $ rightToMaybe <$> f

instance Cleanable [] where
    cleanMaybe = catMaybes

instance Cleanable Maybe where
    cleanMaybe = join

instance Ord a => Cleanable (M.Map a) where
    cleanMaybe = M.fromList . mapMaybe sequence . M.toList


class FetchableRelation a b c where
    fetch :: a -> b -> c

instance FetchableRelation String1 (Dict1 a) (Maybe a) where
    fetch = M.lookup

instance FetchableRelation String2 (Dict2 a) (Maybe a) where
    fetch (k1, k2) m = fetch k1 m >>= M.lookup k2

instance FetchableRelation String3 (Dict3 a) (Maybe a) where
    fetch (k1, k2, k3) m = fetch (k1, k2) m >>= M.lookup k3

instance FetchableRelation String4 (Dict4 a) (Maybe a) where
    fetch (k1, k2, k3, k4) m = fetch (k1, k2, k3) m >>= M.lookup k4

instance FetchableRelation String5 (Dict5 a) (Maybe a) where
    fetch (k1, k2, k3, k4, k5) m = fetch (k1, k2, k3, k4) m >>= M.lookup k5

