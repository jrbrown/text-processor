{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TupleSections #-}

module IO
    ( SingleInput (..)
    , MultiInput (..)
    , SingleOutput (..)
    , MultiOutput (..)
    , readInput
    , writeOutput
    ) where


import Common (FileData, FileName (..))

import Data.List (intercalate)
import System.IO (isEOF)
import Control.Monad.Loops (whileM)


getLines :: IO String
getLines = unlines <$> whileM (not <$> isEOF) getLine


class IOReader a b where
    readInput :: a -> IO b


data SingleInput = StdIn
                 | InFile String
                 deriving (Eq, Ord, Show)

instance IOReader SingleInput String where
    readInput StdIn = getLines
    readInput (InFile file_name) = readFile file_name

instance IOReader SingleInput FileData where
    readInput StdIn = (StdInOut,) <$> getLines
    readInput (InFile file_name) = (Str file_name,) <$> readFile file_name


newtype MultiInput = InFiles [String] deriving (Eq, Ord, Show)

instance IOReader MultiInput [String] where
    readInput (InFiles f_names) = mapM readFile f_names

instance IOReader MultiInput [FileData] where
    readInput (InFiles f_names) = zip (Str <$> f_names) <$> mapM readFile f_names



class IOWriter a b where
    writeOutput :: a -> b -> IO ()

data SingleOutput = StdOut
                  | SOutIdentity
                  | OutFile String
                  deriving (Eq, Ord, Show)

instance IOWriter String SingleOutput where
    writeOutput txt StdOut = putStrLn txt
    writeOutput txt SOutIdentity = putStrLn txt
    writeOutput txt (OutFile name) = writeFile name txt

instance IOWriter FileData SingleOutput where
    writeOutput (_, fbody) StdOut = putStrLn fbody
    writeOutput (_, fbody) (OutFile name) = writeFile name fbody
    writeOutput (StdInOut, fbody) SOutIdentity = putStrLn fbody
    writeOutput (Str fname, fbody) SOutIdentity = writeFile fname fbody


data MultiOutput = OutFiles [String]
                 | MOutIdentity
                 deriving (Eq, Ord, Show)

instance IOWriter [String] MultiOutput where
    writeOutput txts (OutFiles names) = mapM_ (uncurry writeFile) (zip names txts)
    writeOutput txts MOutIdentity = putStrLn $ intercalate "\n" txts

instance IOWriter [FileData] MultiOutput where
    writeOutput fdata (OutFiles names) = mapM_ (uncurry writeFile) (zip names (map snd fdata))
    writeOutput fdata MOutIdentity = mapM_ (`writeOutput` SOutIdentity) fdata


