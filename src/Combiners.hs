module Combiners
  ( Combiner
  , getCombiner
  ) where



import Common (FileData, maybeToRight, FileName (Str), intercalateM)

import Data.SimpleJSON.Encoding (prettyEncodeJson)
import Data.SimpleJSON.Parser (unsafeJsonParse)
import Data.SimpleJSON (toJsonVal, JsonVal (JObj))
import Data.List (intercalate)
import Data.Map as M


type Combiner = String


getCombiner :: String -> Either String ([FileData] -> FileData)
getCombiner name = maybeToRight err val
  where
    err = "\'"++name++"\' not a valid combiner"
    val = M.lookup name strCombinersMap


strCombinersMap :: M.Map String ([FileData] -> FileData)
strCombinersMap = M.fromList
  [ ("concat", fdatConcat)
  , ("concat_with_lb", concatWithLB)
  , ("concat_with_name", concatWithName)
  , ("concat_with_name_lb", concatWithNameLB) 
  , ("combine_json_dicts", combineJSONDicts) ]


fdatConcat :: [FileData] -> FileData
fdatConcat xs = (names, bodies)
  where
    names = mconcat $ fmap fst xs
    bodies = mconcat $ fmap snd xs


concatWithLB :: [FileData] -> FileData
concatWithLB xs = (names, bodies)
  where
    names = intercalateM (Str ",") $ fmap fst xs
    bodies = intercalate "\n" $ fmap snd xs


concatWithName :: [FileData] -> FileData
concatWithName [] = mempty
concatWithName ((name,body):xs) = (name <> Str ",", show name ++ "\n" ++ body) <> concatWithName xs


concatWithNameLB :: [FileData] -> FileData
concatWithNameLB [] = mempty
concatWithNameLB ((name,body):xs) = (name <> Str ",", show name ++ "\n" ++ body ++ "\n")
                                    <> concatWithNameLB xs


combineJSONDicts :: [FileData] -> FileData
combineJSONDicts entries = (intercalateM (Str ",") names, new_body)
  where
    names = fmap fst entries
    bodies = fmap snd entries
    --new_body_lines = fmap (intercalate "\n" . fmap ("    " <>) . lines) bodies
    --new_body = "{\n" ++ intercalate ",\n" new_body_lines ++ "\n}"
    new_body = (prettyEncodeJson . toJsonVal . unions . fmap (unpackJObj . unsafeJsonParse)) bodies
    unpackJObj (JObj map_obj) = map_obj
    unpackJObj _ = error "Not a JSON dict"

