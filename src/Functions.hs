module Functions
  ( getFuncs
  , fdatFuncMap
  , fdatArgFuncMap
  ) where


import Common
  ( (<$$>)
  , (<$$$>)
  , headTailOp
  , headTailOp2
  , numMeanStdDev
  , numMeanStdDevFromMany
  , maybeFail
  , failEither
  , funcOp
  , safeLast
  , safeMaximum
  , safeMaximumBy
  , noInfoStr
  , ks2asFrom2Maps
  , roundFloatStr
  , FileData
  , FileName (StdInOut, Str)
  , applyFuncToFileFameRoot
  , changeFileExt )

import RunInfo
  ( RunInfo (riTask, riVariant, riAttn)
  , Task
  , Variant
  , Attn
  , attnShow
  , allAttns
  , allTasks
  , allVariants
  , getVariants
  , runInfoParse )

import LogParse (nasLogParse, NASLog (NASLog), Block (Block))
import DeepDict (Dict1, Dict2, Dict4, fetch, cleanMaybe, Cleanable)

import Data.SimpleJSON (JsonVal (JNull, JObj, JINum, JENum, JArray), toJsonVal, fromJsonVal)
import Data.SimpleJSON.Transforms (autoPrettyParseFunc)
import Data.SimpleJSON.Encoding (prettyEncodeJson)
import Data.SimpleJSON.Parser (jsonParse)

import qualified Data.Map as M
import Data.List (isInfixOf, intercalate)
import Text.Read (readMaybe)
import Data.Maybe (fromMaybe, mapMaybe)
import Data.Bifunctor (first, second)
import Data.List.Split (splitOn)
import Text.Regex.PCRE ((=~))
import Debug.Trace (trace)


getFuncs :: [String] -> Either String [FileData -> FileData]
getFuncs = ks2asFrom2Maps "function" fdatFuncMap fdatArgFuncMap


fdatFuncMap :: M.Map String (FileData -> FileData)
fdatFuncMap = M.union fdatFuncMap' (fmap second strFuncMap)
  where
    fdatFuncMap' = M.fromList
      [ ("createDBEntry", createDBEntryFromLog)
      , ("processNASLog", processNASLog) ]


strFuncMap :: M.Map String (String -> String)
strFuncMap = M.fromList
  [ ("id", id)
  , ("jsonid", prettyEncodeJson . trace "Done parse" . maybeFail "Bad json parse" . jsonParse)
  , ("testPrefix", ("test" <>))
  , ("testSuffix", (<> "test"))
  , ("valAccs", unlines . fmap show . getEvalAccs)
  , ("finalValAcc", maybe noInfoStr show . safeLast . getEvalAccs) 
  , ("bestValAcc", maybe noInfoStr show . safeMaximum . getEvalAccs)
  , ("lastLine", fromMaybe noInfoStr . safeLast . lines)
  , ("errors", filterErrorLines)
  , ("incrementCheese", autoPrettyParseFunc incrementCheese)
  , ("bestValAccsJson", getBestValAccs) 
  , ("bvajToLatex", bestValAccsJsonToLatex) ]


fdatArgFuncMap :: M.Map String (String -> FileData -> FileData)
fdatArgFuncMap = M.union fdatArgFuncMap' (fmap (second .) strArgFuncMap)  --Thank you HLS, very cool
  where
    fdatArgFuncMap' = M.fromList
      [ ("rename", first . const . Str)
      , ("name_suffix", first . applyFuncToFileFameRoot . flip (<>))
      , ("name_prefix", first . applyFuncToFileFameRoot . (<>)) ]


strArgFuncMap :: M.Map String (String -> String -> String)
strArgFuncMap = M.fromList
  [ ("replace", const)
  , ("prepend", (<>))
  , ("append", flip (<>))
  , ("grepl", grepl)]


grepl :: String -> String -> String
grepl arg = intercalate "\n" . filter (=~ arg) . lines


linesByInfxAll :: [String] -> String -> Bool
linesByInfxAll infxs = funcOp (fmap isInfixOf infxs) and


linesByInfxAny :: [String] -> String -> Bool
linesByInfxAny infxs = funcOp (fmap isInfixOf infxs) or


getItem :: (Read a) => String -> String -> Maybe a
getItem infx = readMaybe . filterComma . (!! 1) . dropWhile (not . isInfixOf infx) . words
  where filterComma txt = if last txt == ',' then init txt else txt


getWithWithout :: (Read a) => String -> [String] -> [String] -> String -> [a]
getWithWithout tgt reqs i_reqs = mapMaybe (getItem tgt) . f2 . f1 . lines
    where
      f1 = filter (linesByInfxAll (tgt : reqs))
      f2 = filter (not . linesByInfxAny i_reqs)

getEvalAccs :: String -> [Double]
getEvalAccs = getWithWithout "acc" ["eval"] []

getTrainAccs :: String -> [Double]
getTrainAccs = getWithWithout "acc" ["train"] ["eval"]

getEvalLoss :: String -> [Double]
getEvalLoss = getWithWithout "loss" ["eval"] []

getTrainLoss :: String -> [Double]
getTrainLoss = getWithWithout "loss" ["train"] ["eval"]


filterErrorLines :: String -> String
filterErrorLines = unlines . filter (linesByInfxAny ["error", "Error", "ERROR"]) . lines


createDBEntryFromLog :: FileData -> FileData
createDBEntryFromLog (file_name, txt) = (file_name, output_body)
  where
    f_name = case file_name of
               StdInOut -> "StdIn"
               Str x    -> (last . splitOn "/" . takeWhile (/='.')) x
    output_body = prettyEncodeJson $ toJsonVal (M.fromList [(f_name, model_data)])
    model_data = M.fromList
      [ ("model_dir", Nothing)
      , ("config", Nothing)
      , ("history", Just history)]
    history = M.fromList
      [ ("train", M.fromList [("loss", getTrainLoss txt), ("accuracy", getTrainAccs txt)])
      , ("validation", M.fromList [("loss", getEvalLoss txt), ("accuracy", getEvalAccs txt)])]


incrementCheese :: JsonVal -> JsonVal
incrementCheese (JObj jmap) = JObj $ M.insert "Cheese" newCheeseVal jmap
  where
    newCheeseVal = case M.lookup "Cheese" jmap of
                     Just (JINum x) -> JINum $ x + 1
                     _              -> JNull
incrementCheese _ = JNull


getBestValAccs :: String -> String
getBestValAccs fbody = prettyEncodeJson json_output
  where
    m_histories = jsonParse fbody >>= fromJsonVal :: Maybe (Dict2 JsonVal)
    histories_m :: Dict2 (Maybe (Dict2 [Double]))
    histories_m = stripDBEntry <$$> maybeFail "Bad JSON parse" m_histories
    histories = cleanMaybe <$> histories_m :: Dict4 [Double]
    m_val_accs = fetch ("history", "validation", "accuracy") <$> histories
    val_accs = maybeFail "Bad dict keys" <$> m_val_accs :: Dict1 [Double]
    val_step_accs = zip ([0..] :: [Int]) <$> val_accs
    best_val_accs :: Dict1 (Int, Double)
    best_val_accs = cleanMaybe $ safeMaximumBy (\(_,v1) (_,v2) -> compare v1 v2) <$> val_step_accs
    best_val_accs_d :: Dict2 JsonVal
    best_val_accs_d = mkDict <$> best_val_accs
      where mkDict (s, v) = M.fromList [ ("step", toJsonVal s), ("acc", toJsonVal v)]
    json_output = toJsonVal best_val_accs_d


stripDBEntry :: JsonVal -> Maybe (Dict2 [Double])
stripDBEntry j = getClean getJEnum <$$$> (getClean getJList <$$> (getClean getJObj <$> getJObj j))
  where
    getClean :: Cleanable f => (JsonVal -> Maybe a) -> f JsonVal -> f a
    getClean getter f_jval = cleanMaybe $ getter <$> f_jval
    getJObj (JObj x) = Just x
    getJObj _ = Nothing
    getJList (JArray x) = Just x
    getJList _ = Nothing
    getJEnum (JENum x) = Just x
    getJEnum _ = Nothing


bestValAccsJsonToLatex :: String -> String
bestValAccsJsonToLatex fbody = latex_str
  where
    m_best_val_accs_d = jsonParse fbody >>= fromJsonVal :: Maybe (Dict2 JsonVal)
    best_val_accs_d = maybeFail "Couldn't parse json" m_best_val_accs_d
    bva_list = M.toList best_val_accs_d
    ri_bvalm_list = first (\str -> maybeFail ("No parse of "++str) (runInfoParse str))
                    <$> bva_list :: [(RunInfo, M.Map String JsonVal)]
    ri_bval_list = second (dict2pair . M.toList) <$> ri_bvalm_list
      where dict2pair [("acc", JENum x), ("step", JINum y)] = (x, y) :: (Double, Integer)
            dict2pair x = error ("Bad keys in dict, should be \'acc\' and \'step\', recieved dict was " ++ show x)

    full_info_list :: [(Task, (Attn, Variant, Double, Integer))]
    full_info_list = [ (riTask ri, (riAttn ri, riVariant ri, x, 200 * y))
                     | (ri, (x, y)) <- ri_bval_list]

    info_list_by_tasks :: [(Task, [(Attn, Variant, Double, Integer)])]
    info_list_by_tasks = filter isEmpty $ partitionByTask <$> allTasks 
      where
        isEmpty (_, xs) = xs /= mempty
        partitionByTask t = (t, filteredInfoByTask t)
        filteredInfoByTask :: Task -> [(Attn, Variant, Double, Integer)]
        filteredInfoByTask t = [x | (t', x) <- full_info_list, t == t']

    latex_str = "ACCURACIES\n\n" ++ getLatexStr fst -- ++ "\n\n\n" ++ "STEPS\n\n" ++ getLatexStr snd
    getLatexStr pick = intercalate "\n\n" task_strings ++ "\n\n" ++ average_by_task_string
      where
        task_strings = (\(t, info_list) ->
                        show t ++ "\n" ++ getLatexLines pick True info_list (getVariants t))
                       <$> info_list_by_tasks
        average_by_task_string = "All\n"
                                 ++ getLatexLines pick False (snd <$> full_info_list) allVariants


getLatexLines :: ((String, String) -> String) -> Bool -> [(Attn, Variant, Double, Integer)]
              -> [Variant] -> String
getLatexLines pick incl_stdev info_list variants = intercalate "\n" (mkLatexLine <$> allAttns)
                                                   ++ average_line
  where
    mkLatexLine attn = show attn ++ " & " ++ intercalate " & " (variantEntries attn) ++ " \\\\"  
    variantEntries attn = fmap (pick . processRuns incl_stdev . varAttnDat attn) variants
    varAttnDat a v = [(acc, step) | (a', v', acc, step) <- info_list, v' == v, a' == a]
    average_line = "\n\\hline\nAverage & " ++ intercalate " & " allVariantEntries ++ " \\\\"
    allVariantEntries = fmap (pick . processRuns incl_stdev . varDat) variants
    varDat v =  [(acc, step) | (_, v', acc, step) <- info_list, v' == v]


processRuns :: Bool -> [(Double, Integer)] -> (String, String)
processRuns _ [] = mempty
processRuns incl_stdev accs_steps = ( mkEntry 100 acc_mean acc_stdDev
                                    , mkEntry 1 step_mean step_stdDev )
  where
    (_, acc_mean, acc_stdDev) = numMeanStdDev $ fst <$> accs_steps
    (_, step_mean, step_stdDev) = numMeanStdDev $ realToFrac . snd <$> accs_steps
    mkEntry mul mean stdDev = "$" ++ m_str ++ s_str ++ "$"
      where m_str = roundFloatStr 1 (mul * mean)
            s_str = if incl_stdev then " \\pm " ++ roundFloatStr 1 (mul * stdDev) else ""


processNASLog :: FileData -> FileData
processNASLog (fname, fbody) = (changeFileExt fname "yaml", yaml_str)
  where
    NASLog run_name blks = failEither $ nasLogParse fbody
    blk_str_list = (\(Block a i) -> let sa = attnShow a in [sa++"_"++show i, sa] ) <$$> blks
    yamlListStr = headTailOp2 yh yt <$> headTailOp yh yt <$$> (yh <$$$> blk_str_list)
    yh = (<>) "- "
    yt = (<>) "  "
    collapse xsss = j $ j <$> j <$$> xsss
    j = intercalate "\n"
    yaml_str = intercalate "\n"
      [ "existing_run_name: \""++run_name++"\""
      , "initial_blocks_per_layer:"
      , collapse $ (<>) "  " <$$$> yamlListStr ]

