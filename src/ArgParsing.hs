module ArgParsing
    ( Config (..)
    , Command (..)
    , One2OneCfg (..)
    , Many2OneCfg (..)
    , Many2ManyCfg (..)
    , SingleFileOp
    , execArgParser
    ) where


import MultiFileOps (MultiFileOp)
import Combiners (Combiner)
import IO
    ( SingleInput (StdIn, InFile)
    , MultiInput (InFiles)
    , SingleOutput (StdOut, SOutIdentity, OutFile) 
    , MultiOutput (OutFiles, MOutIdentity))

import Options.Applicative
import Data.List.Split (splitOn)


commaSplit :: String -> [String]
commaSplit = filter (not . null) . splitOn ","


infoHelp :: Parser a -> InfoMod a -> ParserInfo a
infoHelp parser = info (parser <**> helper)


singleInputP :: Parser SingleInput
singleInputP = stdInP <|> inFileP <|> pure StdIn

stdInP :: Parser SingleInput
stdInP = flag' StdIn
         (  long "stdIn" 
         <> help "Input from standard input (Default)")

inFileP :: Parser SingleInput
inFileP = InFile
      <$> strOption
          (  long "input_file" 
          <> short 'i'
          <> metavar "FILENAME"
          <> help "File to use for input" )


multiInputP :: Parser MultiInput
multiInputP = InFiles . commaSplit
          <$> strOption
              (  long "input_files"
              <> short 'i'
              <> metavar "FILENAME1,FILENAME2,..."
              <> help "Files to use for input" )


multiFileOpsP :: Parser [MultiFileOp]
multiFileOpsP = commaSplit
            <$> strOption
                (  long "operations"
                <> short 'r'
                <> metavar "OPERATION1,OPERATION2,..."
                <> help "Functions or filters to apply to texts" )
            <|> pure []


combinerP :: Parser Combiner
combinerP = strOption
            (  long "combiner"
            <> short 'c'
            <> metavar "COMBINER"
            <> help "Function to combine multiple texts into one" )



type SingleFileOp = String

singleFileOpsP :: Parser [SingleFileOp]
singleFileOpsP = commaSplit
             <$> strOption
                 (  long "functions"
                 <> short 'f'
                 <> metavar "FUNCTION1,FUNCTION2,..."
                 <> help "Functions to apply to text" )
             <|> pure []


singleOutputP :: Parser SingleOutput
singleOutputP = stdOutP <|> outFileP <|> pure SOutIdentity

stdOutP :: Parser SingleOutput
stdOutP = flag' StdOut
          (  long "stdOut" 
          <> help "Output to standard output")

outFileP :: Parser SingleOutput
outFileP = OutFile
      <$> strOption
          (  long "output_file" 
          <> short 'o'
          <> metavar "FILENAME"
          <> help "File to use for output" )


multiOutputP :: Parser MultiOutput
multiOutputP = outFilesP <|> pure MOutIdentity

outFilesP :: Parser MultiOutput
outFilesP = OutFiles . commaSplit
        <$> strOption
            (  long "output_files"
            <> short 'o'
            <> metavar "FILENAME1,FILENAME2,..." 
            <> help "Files to use for output" )


data Config = Config
    { optCommand :: Command 
    , debugConfig :: Bool } 
    deriving (Eq, Ord, Show)

configP :: Parser Config
configP = Config
      <$> commandP
      <*> switch
          (  long "debug_config" 
          <> help "Print config instead of running program")



data Command = One2One One2OneCfg
             | Many2One Many2OneCfg
             | Many2Many Many2ManyCfg
             deriving (Eq, Ord, Show)

commandP :: Parser Command
commandP = subparser
        (  command "one2one" (infoHelp one2oneP (progDesc "Single input and single output mode"))
        <> command "many2one" (infoHelp many2oneP (progDesc "Multi input and single output mode"))
        <> command "many2many" (infoHelp many2manyP (progDesc "Multi input and multi output mode"))
        ) <|> subparser
        (  command "o2o" (infoHelp one2oneP (progDesc "one2one"))
        <> command "m2o" (infoHelp many2oneP (progDesc "many2one"))
        <> command "m2m" (infoHelp many2manyP (progDesc "many2many"))
        <> commandGroup "Abbreviated Commands:" )

one2oneP :: Parser Command
one2oneP = One2One <$> one2oneCfgP

many2oneP :: Parser Command
many2oneP = Many2One <$> many2oneCfgP

many2manyP :: Parser Command
many2manyP = Many2Many <$> many2manyCfgP



data One2OneCfg = One2OneCfg
  { o2oInput :: SingleInput
  , o2oPipeline :: [SingleFileOp]
  , o2oOutput :: SingleOutput }
  deriving (Eq, Ord, Show)

one2oneCfgP :: Parser One2OneCfg
one2oneCfgP = One2OneCfg
          <$> singleInputP
          <*> singleFileOpsP
          <*> singleOutputP


data Many2OneCfg = Many2OneCfg
  { m2oInput :: MultiInput
  , m2oMPipeline :: [MultiFileOp]
  , m2oCombiner :: Combiner
  , m2oSPipeline :: [SingleFileOp]
  , m2oOutput :: SingleOutput }
  deriving (Eq, Ord, Show)

many2oneCfgP :: Parser Many2OneCfg
many2oneCfgP = Many2OneCfg
           <$> multiInputP
           <*> multiFileOpsP
           <*> combinerP
           <*> singleFileOpsP
           <*> singleOutputP


data Many2ManyCfg = Many2ManyCfg
  { m2mInput :: MultiInput
  , m2mPipeline :: [MultiFileOp] 
  , m2mOutput :: MultiOutput }
  deriving (Eq, Ord, Show)

many2manyCfgP :: Parser Many2ManyCfg
many2manyCfgP = Many2ManyCfg
            <$> multiInputP
            <*> multiFileOpsP
            <*> multiOutputP


argParser :: ParserInfo Config
argParser = infoHelp configP
              (  fullDesc
              <> progDesc "Do the things"
              <> header "Haskell File Processor")


execArgParser :: IO Config
execArgParser = execParser argParser

