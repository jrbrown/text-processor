module RunInfo 
    ( RunInfo (..)
    , Task (..)
    , Attn (..)
    , Variant (..)
    , attnShow
    , allAttns
    , allTasks
    , allVariants
    , runInfoParse
    , getVariants
    , attnP
    , ) where


import Text.ParserCombinators.ReadP
import Text.ParserCombinators.SimpleJSON
import Data.Maybe (listToMaybe)
import Data.Char (toLower)


data RunInfo = RunInfo
    { riTask :: Task
    , riAttn :: Attn
    , riVariant :: Variant
    , timeStamp :: String }
    deriving (Eq, Show, Ord)


data Task = TC | TCToken | Listops | Matching deriving (Eq, Ord, Show)
data Attn = BigBird | Linear | Linformer | Local | Longformer | Performer | Reformer
          | Sinkhorn | Sparse | Synthesizer | Transformer deriving (Eq, Ord, Show)
data Variant = Stnd | SlightlyWide | Wide | SuperWide  deriving (Eq, Ord, Show)


attnShow :: Attn -> String
attnShow Linear = "linear_transformer"
attnShow Sparse = "sparse_transformer"
attnShow x = map toLower $ show x


allAttns :: [Attn]
allAttns = [ BigBird, Linear, Linformer, Local, Longformer, Performer, Reformer
           , Sinkhorn, Sparse, Synthesizer, Transformer]


allTasks :: [Task]
allTasks = [TC, TCToken, Listops, Matching]


allVariants :: [Variant]
allVariants = [Stnd, SlightlyWide, Wide, SuperWide]


getVariants :: Task -> [Variant]
getVariants Matching = [Stnd, Wide, SuperWide]
getVariants _ = allVariants


runInfoParse :: String -> Maybe RunInfo
runInfoParse = listToMaybe . fullParseOnly runInfoP


runInfoP :: ReadP RunInfo
runInfoP = do
    tsk <- taskP
    char '_'
    a <- attnP
    char '_'
    (tlvl, v, time) <- choice [tStampVar, varTStamp] <++ stdVarTStamp
    if tlvl
       then if tsk == TC
               then return (RunInfo TCToken a v time)
               else pfail
       else return (RunInfo tsk a v time)


tStampVar :: ReadP (Bool, Variant, String)
tStampVar = do
    time <- timeStampP
    tlvl <- option False (char '_' >> tokenLevelP)
    char '_'
    v <- variantP
    return (tlvl, v, time)


varTStamp :: ReadP (Bool, Variant, String)
varTStamp = do
    tlvl <- option False (tokenLevelP >>= (\b -> char '_' >> return b))
    v <- variantP
    char '_'
    time <- timeStampP
    return (tlvl, v, time)


stdVarTStamp :: ReadP (Bool, Variant, String)
stdVarTStamp = do
    tlvlb <- option False (tokenLevelP >>= (\b -> char '_' >> return b))
    time <- timeStampP
    tlvla <- option False (char '_' >> tokenLevelP)
    return (tlvla || tlvlb, Stnd, time)


timeStampP :: ReadP String
timeStampP = do
    y <- digits
    char '-'
    mo <- digits
    char '-'
    d <- digits
    char '_'
    h <- digits
    char '-'
    mi <- digits
    return (y++"-"++mo++"-"++d++"_"++h++"-"++mi)


tokenLevelP :: ReadP Bool
tokenLevelP = string "token_level" >> return True


taskP :: ReadP Task
taskP = choice [tcP, listopsP, matchingP]

tcP :: ReadP Task
tcP = string "text_classification" >> return TC

listopsP :: ReadP Task
listopsP = string "listops" >> return Listops

matchingP :: ReadP Task
matchingP = string "matching" >> return Matching


attnP :: ReadP Attn
attnP = choice [ bigBirdP, linearP, linformerP, localP, longformerP, performerP, reformerP
               , sinkhornP, sparseP, synthesizerP, transformerP ]

bigBirdP :: ReadP Attn
bigBirdP = string "bigbird" >> return BigBird

linearP :: ReadP Attn
linearP = string "linear_transformer" >> return Linear

linformerP :: ReadP Attn
linformerP = string "linformer" >> return Linformer

localP :: ReadP Attn
localP = string "local" >> return Local

longformerP :: ReadP Attn
longformerP = string "longformer" >> return Longformer

performerP :: ReadP Attn
performerP = string "performer" >> return Performer

reformerP :: ReadP Attn
reformerP = string "reformer" >> return Reformer

sinkhornP :: ReadP Attn
sinkhornP = string "sinkhorn" >> return Sinkhorn

sparseP :: ReadP Attn
sparseP = string "sparse_transformer" >> return Sparse

synthesizerP :: ReadP Attn
synthesizerP = string "synthesizer" >> return Synthesizer

transformerP :: ReadP Attn
transformerP = string "transformer" >> return Transformer


variantP :: ReadP Variant
variantP = option Stnd (choice [slightlyWideP, superWideP] <++ wideP)

slightlyWideP :: ReadP Variant
slightlyWideP = string "slightly_wide" >> return SlightlyWide

superWideP :: ReadP Variant
superWideP = string "super_wide" >> return SuperWide

wideP :: ReadP Variant
wideP = string "wide" >> return Wide

